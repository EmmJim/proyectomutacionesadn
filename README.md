
# Proyecto Mutaciones ADN

Proyecto REST API que detecta si una persona tiene diferencias genéticas basándose en su secuencia de ADN.



## URL API Hosteada en AWS

http://54.174.94.182:4000

Endpoints: 

GET http://54.174.94.182:4000/stats

POST http://54.174.94.182:4000/mutation (Body: String[dna])

## Ejecutar proyecto de manera local

Los pasos para ejecutar el proyecto de manera local son los siguientes.

Clonar el repositorio
```bash
  git clone https://gitlab.com/EmmJim/proyectomutacionesadn.git
```

Instalar todas las dependencias necesarias del proyecto

```bash
  npm run install
```
Agregar el archivo .env

```bash
  .env
```

Para correr el proyecto de manera local

```bash
  npm run dev
```


## Referencia de la API

#### Post para detectar mutación de la cadena

```http
  POST /mutation
```

| Parámetro | Tipo     | Descripción                |
| :-------- | :------- | :------------------------- |
| `dna` | `String[]` | **Requerido**. Arreglo de cadena caracteres NxN |

#### Get para obtener estadisticas de los ADN verificados

```http
  GET /stats
```

| Parámetro | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| N/A     |  |  |

#### hasMutation(matrix)

Toma un un arreglo de cadena caracteres que representan cada fila de una tabla de NxN
con la secuencia del ADN. Las letras de los caracteres solo pueden ser: A, T, C, G; las cuales representan
cada base nitrogenada del ADN. Si se encuentra más de una secuencia de cuatro letras iguales, de forma oblicua,
horizontal o vertical, entonces existe mutación.
La función retorna true o false.


## Autor

- [@EmmJim](https://www.github.com/EmmJim)

