const mongoose = require('mongoose');

const dbConnection = async() => {
    try {
        await mongoose.connect(process.env.MONGODB_URI, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        })
        console.log('Database online');
    } catch (error) {
        console.log(error);
        throw new Error('There was an error trying to connect to the database');
    }
}

module.exports = {
    dbConnection
}