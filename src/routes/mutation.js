const {Router} = require('express');
const { detectMutation } = require('../controllers/mutation');

const router = Router();

router.post('/', detectMutation);

module.exports = router;