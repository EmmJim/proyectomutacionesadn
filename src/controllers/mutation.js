const {hasMutation} = require('../helpers/hasMutationFunction');
const Dna = require('../models/dna'); 

const detectMutation = async(req, res) => {
    const {dna} = req.body;

    try {
        const dnaExists = await Dna.findOne({dna});
    
        if(dnaExists){
            return res.status(403).json({msg: 'DNA Already Exists, try another one'});
        }

        const result = hasMutation(dna);

        const newDna = await Dna.create({
            dna,
            result
        })

        if(result){
            return res.status(200).json(newDna);
        }
        return res.status(403).json(newDna);
    } catch (error) {
        return res.status(500).json({msg: 'Something went wrong'})
    }
    
}


module.exports = {
    detectMutation,
}