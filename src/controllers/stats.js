const Dna = require('../models/dna'); 

const getStats = async(req, res) => {
    try {
        const [count_mutations, count_no_mutation] = await Promise.all([
            await Dna.count({result: true}),
            await Dna.count({result: false})
        ]);
        
        const stats = {
            count_mutations,
            count_no_mutation,
            ratio: count_mutations > 0 && count_no_mutation > 0 ? count_mutations / count_no_mutation : 0
        }

        return res.status(200).json(stats);
    } catch (error) {
        return res.status(500).json({msg: 'Something went wrong'})
    }
}

module.exports = {
    getStats
}