function hasMutation(matrix) {
    const n = matrix.length;

    for (let i = 0; i < n; i++) {
        let countH = 1, countV = 1;

        for (let j = 1; j < n; j++) {
            if (matrix[i][j] === matrix[i][j - 1]) {
                countH++;
                if (countH === 4) {
                    return true;
                }
            } else {
                countH = 1;
            }
            if (matrix[j][i] === matrix[j - 1][i]) {
                countV++;
                if (countV === 4) {
                    return true;
                }
            } else {
                countV = 1;
            }
        }
    }
    
    for (let i = 0; i < n - 3; i++) {
        for (let j = 0; j < n - 3; j++) {
            if (matrix[i][j] === matrix[i + 1][j + 1] &&
                matrix[i + 1][j + 1] === matrix[i + 2][j + 2] &&
                matrix[i + 2][j + 2] === matrix[i + 3][j + 3]) {
                return true;
            }
            if (matrix[i][j + 3] === matrix[i + 1][j + 2] &&
                matrix[i + 1][j + 2] === matrix[i + 2][j + 1] &&
                matrix[i + 2][j + 1] === matrix[i + 3][j]) {
                return true;
            }
        }
    }
    return false;
}

module.exports = {
    hasMutation
}