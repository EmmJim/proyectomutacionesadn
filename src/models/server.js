const express = require('express');
const cors = require('cors');
const helmet = require('helmet');
const { dbConnection } = require('../database/config');

class Server {
    constructor(){
        this.app = express();
        this.port = process.env.PORT || 4000;

        this.paths = {
            mutation: '/mutation',
            stats: '/stats'
        }

        this.connectToDB();
        
        this.middlewares();

        this.routes();
    }

    async connectToDB() {
        await dbConnection();
    }

    middlewares(){
        this.app.use(helmet());
        this.app.use(cors());

        this.app.use(express.json());
        this.app.use(express.static('public'));
    }

    routes(){
        this.app.use(this.paths.mutation, require('../routes/mutation'));
        this.app.use(this.paths.stats, require('../routes/stats'));
    }

    listen(){
        this.app.listen(this.port, () => {
            console.log(`Server running on PORT: ${this.port}`);
        });
    }
}

module.exports = Server;