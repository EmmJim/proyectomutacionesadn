const {Schema, model}  =require('mongoose');

const DnaSchema = Schema({
    dna: [String],
    result: {
        type: Boolean,
        required: true
    }
});

DnaSchema.methods.toJSON = function(){
    const {__v, ...data} = this.toObject();
    return data;
}

module.exports = model('Dna', DnaSchema);