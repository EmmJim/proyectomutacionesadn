const { detectMutation } = require('../../controllers/mutation');
const { hasMutation } = require('../../helpers/hasMutationFunction');
const Dna = require('../../models/dna');

jest.mock('../../models/dna');
jest.mock('../../helpers/hasMutationFunction');

describe('detectMutation controller', () => {
    it('should return 403 if DNA already exists', async () => {
        const dna = [
            "ATGCGA",
            "CAGTGC",
            "TTATAT",
            "AGAAGG",
            "CCGCCA",
            "TCACTA"
        ];

        Dna.findOne.mockResolvedValue({ dna });

        const req = { body: { dna } };
        const res = {
            status: jest.fn(() => res),
            json: jest.fn(),
        };

        await detectMutation(req, res);

        expect(res.status).toHaveBeenCalledWith(403);
        expect(res.json).toHaveBeenCalledWith({ msg: 'DNA Already Exists, try another one' });
    });

    it('should return 200 with a valid DNA mutation', async () => {
        const dna = [
            "ATGCGA",
            "CAGTGC",
            "TTATAT",
            "AGAAGG",
            "CCGCCA",
            "TCACTA"
        ];

        const result = true;

        Dna.findOne.mockResolvedValue(null);
        hasMutation.mockReturnValue(result);
        Dna.create.mockResolvedValue({ dna, result });

        const req = { body: { dna } };
        const res = {
            status: jest.fn(() => res),
            json: jest.fn(),
        };

        await detectMutation(req, res);

        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).toHaveBeenCalledWith({ dna, result });
    });

    it('should return 403 with an invalid DNA mutation', async () => {
        const dna = [
            "ATGCGA",
            "CAGTGC",
            "TTATTT",
            "AGACGG",
            "GCGTCA",
            "TCACTA"
        ];

        const result = false;

        Dna.findOne.mockResolvedValue(null);
        hasMutation.mockReturnValue(result);
        Dna.create.mockResolvedValue({ dna, result });

        const req = { body: { dna } };
        const res = {
            status: jest.fn(() => res),
            json: jest.fn(),
        };

        await detectMutation(req, res);

        expect(res.status).toHaveBeenCalledWith(403);
        expect(res.json).toHaveBeenCalledWith({ dna, result });
    });

    it('should return 500 if an error occurs', async () => {
        const dna = [
            "ATGCGA",
            "CAGTGC",
            "TTATAT",
            "AGAAGG",
            "CCGCCA",
            "TCACTA"
        ];

        Dna.findOne.mockRejectedValue(new Error('Database error'));

        const req = { body: { dna } };
        const res = {
            status: jest.fn(() => res),
            json: jest.fn(),
        };

        await detectMutation(req, res);

        expect(res.status).toHaveBeenCalledWith(500);
        expect(res.json).toHaveBeenCalledWith({ msg: 'Something went wrong' });
    });
});
