const Dna = require('../../models/dna');
const { getStats } = require('../../controllers/stats');

describe('getStats', () => {
    it('should return the correct statistics when there are mutations and no mutations', async () => {
        jest.spyOn(Dna, 'count').mockImplementation(async (query) => {
            if (query.result === true) {
                return 10; // number of documents with result: true
            } else if (query.result === false) {
                return 20; // number of documents with result: false
            }
        });

        const expectedStats = {
            count_mutations: 10,
            count_no_mutation: 20,
            ratio: 0.5,
        };

        const req = {};
        const res = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn(),
        };

        await getStats(req, res);

        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).toHaveBeenCalledWith(expectedStats);
    });

    it('should return an error message and status 500 when there is an error', async () => {
        jest.spyOn(Dna, 'count').mockImplementation(async () => {
            throw new Error('Database error');
        });

        const req = {};
        const res = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn(),
        };

        await getStats(req, res);

        expect(res.status).toHaveBeenCalledWith(500);
        expect(res.json).toHaveBeenCalledWith({ msg: 'Something went wrong' });
    });
});
