const {hasMutation} = require('../../helpers/hasMutationFunction');

describe("hasMutation", () => {
    it("should return false for empty matrix", () => {
        expect(hasMutation([])).toBe(false);
    });

    it("should return false for matrix with no mutations", () => {
        const matrix = [
        ["A", "C", "G", "T"],
        ["C", "A", "A", "G"],
        ["G", "T", "A", "C"],
        ["T", "G", "C", "C"],
        ];
        expect(hasMutation(matrix)).toBe(false);
    });

    it("should return true for matrix with horizontal mutation", () => {
        const matrix = [
        ["A", "C", "G", "T"],
        ["C", "A", "T", "G"],
        ["G", "G", "G", "G"],
        ["T", "G", "C", "A"],
        ];
        expect(hasMutation(matrix)).toBe(true);
    });

    it("should return true for matrix with vertical mutation", () => {
        const matrix = [
        ["A", "C", "G", "T"],
        ["C", "A", "T", "G"],
        ["G", "T", "A", "C"],
        ["T", "G", "A", "A"],
        ];
        expect(hasMutation(matrix)).toBe(true);
    });

    it("should return true for matrix with diagonal mutation", () => {
        const matrix = [
        ["A", "C", "G", "T"],
        ["C", "A", "T", "G"],
        ["G", "T", "G", "C"],
        ["T", "G", "C", "G"],
        ];
        expect(hasMutation(matrix)).toBe(true);
    });
});